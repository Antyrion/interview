import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { tap } from 'rxjs/operators';
import { trigger, style, transition, animate, keyframes, query, stagger } from '@angular/animations';


@Component({
  selector: 'app-commits',
  templateUrl: './commits.component.html',
  styleUrls: ['./commits.component.scss']

})
export class CommitsComponent implements OnInit {

  commit$: Object;

  constructor(private route: ActivatedRoute, private data: DataService) {
    this.route.params.subscribe( params => this.commit$ = params.sha );
   }

  ngOnInit() {
    this.data.getCommit(this.commit$).subscribe( 
      data => {this.commit$ = data;
      console.log(this.commit$);
      }
    );
  }

}
