import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get('https://jsonplaceholder.typicode.com/users');
  }

  getUser(userId) {
    return this.http.get('https://jsonplaceholder.typicode.com/users/' + userId);
  }

  getPosts() {
    return this.http.get('https://jsonplaceholder.typicode.com/posts');
  }

  getRepositories() {
    return this.http.get('https://api.github.com/orgs/comapi/repos');
  }
  getCommits() {
    return this.http.get('https://api.github.com/repos/comapi/public_assets/commits');
  }
  getCommit(sha) {
    return this.http.get('https://api.github.com/repos/comapi/public_assets/commits/' + sha);
  }
 
}
